#include <iostream>
#include "MathFunc.h"
#pragma comment(lib,"MyStaticLib.lib")

using namespace std;

int main()
{
    double a = 1.7;
    int b = 17.07;
    
    cout << "Call the function from static library!" << endl; 
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "a + b = " << MathFunc::MyMathFunc::Add(a, b) << endl;
    cout << "a - b = " << MathFunc::MyMathFunc::Sub(a, b) << endl;

    return 0;
}
