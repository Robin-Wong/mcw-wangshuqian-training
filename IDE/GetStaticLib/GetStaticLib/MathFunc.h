// MathFunc.h

namespace MathFunc
{
    class MyMathFunc
    {
    public:
        // Returns a + b
        static __declspec(dllexport) double Add(double a, double b);

        // Returns a - b
        static __declspec(dllexport) double Sub(double a, double b);
    };
}
